all: serveur client

serveur: code_source/serveur.c
	gcc -o serveur code_source/serveur.c code_source/liste_fonctions.c -lm
client: code_source/client.c
	gcc -o client code_source/client.c -lm
