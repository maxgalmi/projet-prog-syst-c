#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>
#include <fcntl.h>
#include "constantes.h"
#include "serialisation.h"


int appel_externe(const char *f, unsigned short argc, struct arg *argv){


   char BUFF[1024];
    struct sockaddr_un sun;
    int fd, bufptr, bufend, rc;

    memset(&sun, 0, sizeof(sun));
    sun.sun_family = AF_UNIX;
    strncpy(sun.sun_path, getenv("HOME"), sizeof(sun.sun_path)-1);
    strncat(sun.sun_path, "/", sizeof(sun.sun_path)-1);
    strncat(sun.sun_path, ".soquette", sizeof(sun.sun_path)-1);

    fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if(fd < 0) {
        perror("socket");
        exit(1);
    }

    rc = connect(fd, (struct sockaddr*)&sun, sizeof(sun));
    if(rc < 0) {
        perror("connect");
        exit(1);
    }
     
    write(fd, serialize_string((char *)f), 100);
      read(fd,BUFF,1024);
      if(deserialize_int(BUFF)==FONCTION_INCONNUE)
	return FONCTION_INCONNUE;
       
      write(fd, serialize_int(argc-1), 10);
       read(fd,BUFF,1024);
      write(fd, serialize_int(argv[0].type),10);
      read(fd,BUFF,1024);
      int i;
      for(i =1; i < argc; i++){
	 
	write(fd, serialize_int(argv[i].type),10);
	  read(fd,BUFF,1024);
	if(argv[i].type==TYPE_INT){
	  write(fd, serialize_int(*(int *)argv[i].arg),10);
	   read(fd,BUFF,1024);
	}else {
	  printf("arg %s  \n", (char *)argv[i].arg );
	   write(fd, serialize_string((char *)argv[i].arg),100);
	    read(fd,BUFF,1024);
	}

      }
      
      read(fd,BUFF,1024);
     
      if(deserialize_int(BUFF)==MAUVAIS_ARGUMENTS){
	write(fd,"recu",10);
	return MAUVAIS_ARGUMENTS;
      }

      int pid = getpid();
      if(fork()>0){ //pere
	write(fd,"recu",10);
	 read(fd,BUFF,1024);
	if(argv[0].type==TYPE_INT){
	  int r = deserialize_int(BUFF);	  
	  * (int *)argv[0].arg = r;
	}else if (argv[0].type == TYPE_STRING){
	    char* r = deserialize_string(BUFF);
	    printf("resultat test %s \n", r);
	    argv[0].arg = malloc((int)BUFF[1]*sizeof(char));
	    char * c;
	    c =  argv[0].arg ;   
	    strcpy(c, r);
	    printf("c test %s \n", c);
	    printf("a0 test %s \n", (char*) argv[0].arg);	  
	    
	}
      return APPEL_OK;
      }else {// fils
	sleep(5);
    	if(kill(pid,SIGKILL)>=0){
	  return PAS_DE_REPONSE;
	}else exit(0);
      }
       
}


int main(){
  
  /* int arg1 = 5;
  int arg2 = 10;
  int result;
  arg arguments[3] = {{TYPE_INT, &result},{TYPE_INT,&arg1},{TYPE_INT,&arg2}};
  int retour = appel_externe("addition",3,arguments);
  printf("%d resultat \n", result);*/
  
 /*  int arg1 = 5;

  int result;
  arg arguments[2] = {{TYPE_INT, &result},{TYPE_INT,&arg1}};
  int retour = appel_externe("calcul_factoriel",2,arguments);
  printf("%d resultat \n", result);*/



 char * arg1 = "a1";
  char * arg2 = " a2";
    char* result;
//result= malloc(100*sizeof(char)) ;
  arg arguments[3] = {{TYPE_STRING, &result},{TYPE_STRING,arg1},{TYPE_STRING,arg2}};
  int retour = appel_externe("concat",3,arguments);
  printf("resultat %s \n", result);
     printf("%d retour \n", retour);
   	 return retour;



  /*char* path = "/tmp/test.txt";
char* s = "test";
  arg arguments[3] = {{TYPE_VOID, NULL},{TYPE_STRING,s},{TYPE_STRING,path}};
  int retour = appel_externe("ecriture",3,arguments);

     printf("%d retour \n", retour);
     return retour;*/

/*
  char* path = "test.txt";
  char* s = "test";
  int file = open(path,O_CREAT | O_RDWR, S_IRWXU);
    if(file < 0) {
        perror("open");
        exit(1);
    }
  arg arguments[3] = {{TYPE_VOID, NULL},{TYPE_STRING,s},{TYPE_INT,&file}};
  int retour = appel_externe("write_to_file2",3,arguments);
*/
     printf("%d retour \n", retour);
   	 return retour;
   
  
} 
      
    

	
	
