//
//  liste_fonctions.h
//  
//
//  Created by Valens Iraguha
//
//



#include <stdio.h>
#include "constantes.h"
int addition(int a, int b) ;
int soustraction(int a, int b);
int multiplication(int a, int b);
int division(int a, int b);
int calcul_factoriel(int nombre);
void lecture(char* nom_fichier);
void ecriture(const char *chaine, char *nom_fichier) ;
char* concat(char* str1, char* str2);



//----------------------------------------------
fonction* recherche(Liste *liste, char* s);
void insertion(Liste *liste, fonction* nvf);
Liste *initialisation(fonction* fonc);
void err_sys(const char* x) ;