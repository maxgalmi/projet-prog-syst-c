#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include "serialisation.h"
#include "liste_fonctions.h"




 fonction p = {"addition",TYPE_INT,2,{TYPE_INT,TYPE_INT}};
 fonction s = {"soustraction",TYPE_INT,2,{TYPE_INT,TYPE_INT}};
 fonction m = {"multiplication",TYPE_INT,2,{TYPE_INT,TYPE_INT}};
 fonction d = {"division",TYPE_INT,2,{TYPE_INT,TYPE_INT}};
 fonction fact =  {"calcul_factoriel",TYPE_INT,1,{TYPE_INT}};
  
  
fonction c = {"concat",TYPE_STRING,2,{TYPE_STRING,TYPE_STRING}};
fonction e = {"ecriture",TYPE_VOID,2,{TYPE_STRING,TYPE_STRING}};
fonction lect = {"lecture",TYPE_VOID,1,{TYPE_STRING}};


//fonction w2 = {"write_to_file2",TYPE_VOID,2,{TYPE_STRING,TYPE_INT}};

int main(){

  Liste* l = initialisation(&p);
  insertion(l,&s);
  insertion(l,&m);
  insertion(l,&d);
  insertion(l,&fact);
  insertion(l,&c);
  insertion(l,&e);
  insertion(l,&lect);
  
  
   struct sockaddr_un addr;
   addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, getenv("HOME"), sizeof(addr.sun_path)-1);
    strncat(addr.sun_path, "/", sizeof(addr.sun_path)-1);
    strncat(addr.sun_path, ".soquette", sizeof(addr.sun_path)-1);

  int s = socket(AF_UNIX,SOCK_STREAM,0);
  if (s<0)
    err_sys ( " socket error " ) ;

    
   if (unlink(addr.sun_path)<0 &&errno != ENOENT)
     err_sys ("unlink error ");

  if( bind(s,(struct sockaddr *)&addr, sizeof(addr))<0)
      err_sys ( " bind error " ) ;
  
  if (listen(s,100)<0){
    err_sys ( " listen error " ) ;
  }

  while(TRUE){
  int conn = accept(s, NULL, NULL);
  //if(fork()<0){
  if (conn< 0) err_sys("listen error");
    printf("... \n");
        char BUFF[1024];

	read(conn, BUFF, 1024);
	char* c = deserialize_string(BUFF);

	fonction* f = recherche(l,c);
	if (f==NULL){
	  
	  write(conn,serialize_int(FONCTION_INCONNUE),10);
	}
	else{ 
	   
	  write(conn,serialize_int(APPEL_OK),10);
	}
	read(conn,BUFF,1024);
	int argc = deserialize_int(BUFF);
	write(conn,"recu",10);
	arg arguments[argc];
	read(conn,BUFF,1024);
	write(conn,"recu",10);
	int type_retour = deserialize_int(BUFF);
        printf("test \n");
	int i;
	printf("argc %d \n",argc);
	for(i = 0; i < argc; i++){
	  printf("i %d \n",i);
	  read(conn,BUFF,1024);
	  write(conn,"recu",10);
	  int type  =  deserialize_int(BUFF);
	    read(conn,BUFF,1024);
	    write(conn,"recu",10);
	    if(type == TYPE_INT){
	      int* n = malloc(sizeof(int));
	     * n =   deserialize_int(BUFF);
	      printf("recu %d \n",*n);
	      //arg a =  {type,(void *)&n};
	      // printf("arg %d \n",*(int *)a.arg);
	      arguments[i].type = type;
	       arguments[i].arg = (void *) n;
	       
	    }else{
	      //char *arg_c = malloc((int)BUFF[1]*sizeof(char)) ;
	     char* arg_c = deserialize_string(BUFF);
	     //strcpy(arg_c,arg_c2);
	       printf("arg_c %s \n", arg_c);
	       arguments[i].type = type;
	       arguments[i].arg = (void *)arg_c;
	    }   
	    	
	}

	int appel_ok = TRUE;
	printf("argc %d \n",argc);
	if(argc!=f->argc){
	   printf("MAUVAIS_ARGUMENTS \n");
	   write(conn,serialize_int(MAUVAIS_ARGUMENTS),100);
	   appel_ok = FALSE;
	}else{
	  for(i = 0; i < argc; i++){
	    if(arguments[i].type != f->type_arguments[i]){
	       write(conn,serialize_int(MAUVAIS_ARGUMENTS),100);
	       appel_ok = FALSE;
	       break;
	    }
	  }
	}

	if(appel_ok){
	   
	   write(conn,serialize_int(APPEL_OK),1024);
	   read(conn,BUFF,1024);
	
	
	if(strcmp("addition",f->nom)==0){
	  int result = addition(*(int *)arguments[0].arg,*(int *)arguments[1].arg);
	  write(conn,serialize_int(result),100);
	}else if(strcmp("soustraction",f->nom)==0){
	  int result = soustraction(*(int *)arguments[0].arg,*(int *)arguments[1].arg);
	  write(conn,serialize_int(result),100);
	}else if(strcmp("multiplication",f->nom)==0){
	  int result = multiplication(*(int *)arguments[0].arg,*(int *)arguments[1].arg);
	  write(conn,serialize_int(result),100);
	}else if(strcmp("division",f->nom)==0){
	  int result = division(*(int *)arguments[0].arg,*(int *)arguments[1].arg);
	  write(conn,serialize_int(result),100);
	}else if(strcmp("calcul_factoriel",f->nom)==0){
	  int result = calcul_factoriel(*(int *)arguments[0].arg);
	  write(conn,serialize_int(result),100);
	}
	else if(strcmp("concat",f->nom)==0){
	  char* result = concat((char *)arguments[0].arg,(char *)arguments[1].arg);
	    printf("%s resultat \n", result);
	  write(conn,serialize_string(result),100);
	}else if(strcmp("ecriture",f->nom)==0){
	  ecriture((char *)arguments[0].arg,(char *)arguments[1].arg);
	   write(conn,"terminé",100);
	}else if(strcmp("lecture",f->nom)==0){
	  lecture((char *)arguments[0].arg);
	   write(conn,"terminé",100);
	}
	
	}
  
  }
}

