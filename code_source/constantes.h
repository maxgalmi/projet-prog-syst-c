#include <stdio.h>
#include <stdlib.h>

#define FONCTION_INCONNUE -1
#define APPEL_OK 0
#define MAUVAIS_ARGUMENTS 1
#define PAS_DE_REPONSE 2
#define TYPE_VOID 3
#define TYPE_INT 4
#define TYPE_STRING 5


typedef struct Fonction
{
  char* nom;
  int retour;
  int argc;
  int type_arguments[];
} fonction;


typedef struct arg arg;
struct arg{
  int type;
  void *arg;
};
  

typedef struct Element Element;
struct Element
{
    fonction *f;
    Element *suivant;
};


typedef struct Liste Liste;
struct Liste
{
    Element *premier;
};


