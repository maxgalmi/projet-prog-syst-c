//
//  liste_fonctions.c
//  
//
//  Created by Valens Iraguha
//
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "liste_fonctions.h"

#define TAILLE 512

void err_sys(const char* x) 
{ 
    perror(x); 
    exit(1); 
}

int addition(int a, int b) {
  //sleep(5);
  //sleep(3)
    int c;
    c = a + b;
    return c;
}

int soustraction(int a, int b) {
    int c;
    c = a - b;
    return c;
}

int multiplication(int a, int b) {
    int c;
    c = a * b;
    return c;
}

int division(int a, int b) {
    int c;
    c = a / b;
    return c;
}

int calcul_factoriel(int nombre) {
  int factoriel;
    if (nombre != 0) {
        factoriel = nombre;
        do {
            nombre = nombre - 1;
            factoriel = factoriel * nombre;
        }
        while (nombre > 1);
    }
    else {
        factoriel = 0;
    }
    return factoriel;
}

void lecture(char* nom_fichier) {
    
    FILE *fichier;
    char ligne[TAILLE];
    char** tableau_lignes;
    int i;
    
    // ouverture du fichier en lecture
    fichier = fopen(nom_fichier, "r");
    
    if(fichier == NULL) {
        printf("Impossible d'ouvrir le fichier %s \n", nom_fichier);
        exit(1);
    }
    
    i = 0;
    
    // recuperation de lignes du fichier
    while(fgets(ligne, TAILLE, fichier) != NULL) {
	tableau_lignes[i] = malloc(strlen(ligne)*sizeof(char));
        tableau_lignes[i] = ligne;
        i++;
    }
    
    // fermeture du fichier
    if(fclose(fichier) == EOF) {
        printf("Probleme de fermeture du fichier %s \n", nom_fichier);
    }

}

void ecriture(const char *chaine, char *nom_fichier) {
    
    FILE *fichier;
    
    // ouverture du fichier en ecriture
    fichier = fopen(nom_fichier, "w");
    
    if(fichier == NULL){
        printf("Impossible d'ouvrir le fichier %s \n", nom_fichier);
        exit(1); // pour quitter le programme
    }
    
    fseek(fichier, 0, SEEK_END); // on place le curseur à la fin du fichier
    
    fputs(chaine, fichier); // on ajoute la chaine de caractères à la fin du fichier
    
    // fermeture du fichier
    if(fclose(fichier) == EOF) {
        printf("Probleme de fermeture du fichier %s \n", nom_fichier);
    }
}


char* concat(char* str1, char* str2){
  char * str3 = (char *) malloc(1 + strlen(str1)+ strlen(str2) );
      strcpy(str3, str1);
      strcat(str3, str2);
      return str3;
}



//----------------------------------------------------------------------------------------


Liste *initialisation(fonction* fonc)
{
    Liste *liste = malloc(sizeof(*liste));
    Element *element = malloc(sizeof(*element));
    if (liste == NULL || element == NULL)
    {
        exit(EXIT_FAILURE);
    }
    element->f = fonc;
    element->suivant = NULL;
    liste->premier = element;
    return liste;
}

void insertion(Liste *liste, fonction* nvf)
{
    /* Création du nouvel élément */
    Element *nouveau = malloc(sizeof(*nouveau));
    if (liste == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    nouveau->f = nvf;

    /* Insertion de l'élément au début de la liste */
    nouveau->suivant = liste->premier;
    liste->premier = nouveau;
}

fonction* recherche(Liste *liste, char* s){
    if (liste == NULL)
   {
        exit(EXIT_FAILURE);
    }

    Element *actuel = liste->premier;

    while (actuel != NULL)

    {
      if( strcmp(s, actuel->f->nom) ==0){
        printf("trouvé \n ");
	return actuel->f;
      }
        actuel = actuel->suivant;

    }

    printf("non trouve \n");
    return NULL;

}


