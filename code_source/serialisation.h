#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define FALSE 0
#define TRUE 1

unsigned char* serialize_int(int x){
	int negative = FALSE;
	if(x == 0) {
	  unsigned char*c = malloc(3);
	  c[0] = 0x01; c[1] = 0x01; c[2] = '0';
	  return c;
	}else if(x < 0){
	  // printf("negatif \n");
	 negative = TRUE;x = -x;
	  // printf("%d \n",x);
	}
	
	
	int nDigits = floor(log10(abs(x)))+1;   

	unsigned char* b= (unsigned char*)&nDigits;
	
	unsigned char a[] = {0x01,b[0]};
	unsigned char s[nDigits+1]; // Nombre maximal de chiffres + 1
 
    sprintf(s, "%d", x); // Conversion de l'entier
 
	unsigned char* c;
	int n = 2;
	if(negative) n = 3;
	c = malloc(n+nDigits);
	//  printf("%d \n",n);
	if (negative) c[2] = '-';
	int i;
	for(i = 0; i<nDigits+n;i++){
		if(i<2)
			c[i] = a[i];
		else if(i!=2||(i==2&&!negative))	
			c[i]=s[i-n];
	}
	//  printf("%c \n",c[2]);

	
  
	return c;

}

unsigned char* serialize_string(char* x){  

	int l = strlen(x);
	unsigned char* b= (unsigned char*)&l;
	
	
	unsigned char a[] = {0x01,b[0]};
	
 

	unsigned char* c;
	c = malloc(2+l);

	int i;
	for(i = 0; i<l+2;i++){
		if(i<2)
			c[i] = a[i];
		else	
			c[i]=x[i-2];
	}

	
  
	return c;

}

int deserialize_int(unsigned char * c){
	if(c[2]=='-') return - atoi(c+3);
	return atoi(c+2);
}

char* deserialize_string(unsigned char * c){
	int l = (int)c[1];
	char* a = malloc(l*sizeof(char));
	strcpy(a,c+2);
	return a;
}